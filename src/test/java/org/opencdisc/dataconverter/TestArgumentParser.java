/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter;

import com.martiansoftware.jsap.JSAPException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.rules.ExpectedException;
import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.data.DataSource;
import org.opencdisc.validator.data.EnhancedSasTransportDataSource;
import org.opencdisc.validator.data.InvalidDataException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import static org.junit.Assert.*;

public class TestArgumentParser {

    //TODO Figure out how to make this variable list cleaner...

    String outputPath = "src/test/resources/testOutput";

    /** String of path with no files. */
    String noFilesString = new File("src/test").getAbsolutePath();
    /**
     * Path containing no xpt files.
     * I have to convert it into an absolute path for testing,
     * because the tested method does not convert the path.
     */
    String[] noFilesPath = new String[]{"-s " + noFilesString, "-o src/test/resources/testOutput"};

    /** Arguments with a good source path. */
    String[] goodSourcePath = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput"};
    /** Arguments with a bad source path. */
    String[] badSourcePath  = new String[]{"-s src/test/resourcezz", "-o src/test/resources/testOutput"};


    /** Arguments with a good output path. */
    String[] goodOutputPath = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput"};
    /** Arguments with a bad output path. */
    String[] badOutputPath  = new String[]{"-s src/test/resources", "-o src/test/resources/testCreateOutput"};


    /** Argumenta with a good source type. */
    String[] goodSourceType = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput"};
    /** Arguments with a bad source type. */
    String[] badSourceType  = new String[]{"-s src/test/resources", "-i xxx", "-o src/test/resources/testOutput"};


    /** Arguments with a good output type. */
    String[] goodOutputType = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput", "-e csv"};
    /** Arguments with a bad output type. */
    String[] badOutputType  = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput", "-e ggg"};


    /** Arguments with duplicate paths. */
    String[] duplicatePath = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput"};


    /** Arguments with a single file as the source. */
    String[] singleFilePath = new String[]{"-s src/test/resources/ae.xpt", "-o src/test/resources/testOutput"};


    /** Path with no define.xml document but a file path */
    String noDefinePathFile = new File("src/test/resources/ae.xpt").getAbsolutePath();
    /** Arguments that use the define arg but points to a file */
    String[] badDefineArgsFile = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput", "-e xml", "-d " + noDefinePathFile};


    /** Path with no define.xml document but is a directory */
    String noDefinePathDirectory = new File("src/test/resources").getAbsolutePath();
    /**Arguments that use the define arg but point to a directory */
    String[] badDefineArgsDirectory = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput", "-d " + noDefinePathDirectory};


    String definePath = new File("src/test/resources/defines/define.xml").getAbsolutePath();
    /** Arguments with a good source path. */
    String[] goodDefinePathArgs = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput", "-e xml", "-d " + definePath};

    String defineMissingDatasetPath = new File("src/test/resources/defines/defineMissingDataset.xml").getAbsolutePath();
    /** Arguments with a good source path. */
    String[] badDefineMissingDatasetPathArgs = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput", "-e xml", "-d " + defineMissingDatasetPath};

    String defineMissingVariablePath = new File("src/test/resources/defines/defineMissingVariable.xml").getAbsolutePath();
    /** Arguments with a good source path. */
    String[] badDefineMissingVariablePathArgs = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput", "-e xml", "-d " + defineMissingVariablePath};


    String[] noDefineORConfigArgs = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput", "-e xml"};


    String configPath = new File("src/test/resources/SDTM 3.1.3 (FDA).xml").getAbsolutePath();
    /** Arguments with a good source path. */
    String[] goodConfigPathArgs = new String[]{"-s src/test/resources", "-o src/test/resources/testOutput", "-e xml", "-c " + configPath};


    /** Used to expect certain messages inside an exception when thrown. */
    @Rule
    public ExpectedException exception = ExpectedException.none();

    /**
     * Tests validation of source paths by passing a good path.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     */
    @Test
    public void testValidateSourcePathGood() throws IOException, JSAPException {
        ArgumentParser parser = new ArgumentParser(goodSourcePath);
        parser.validateSourcePath(parser.source);
    }

    /**
     * Tests validation of source paths py passing a bad path.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     */
    @Test
    public void testValidateSourcePathBad() throws IOException, JSAPException {
        String badSourcePathString = new File("src/test/resourcezz").getAbsolutePath();

        exception.expectMessage(String.format("Your specified source path \"%s\"" +
                " does not exist. Please verify the path and try again.", badSourcePathString));

        ArgumentParser parser = new ArgumentParser(badSourcePath);
        parser.validateSourcePath(parser.source);
    }

    /**
     * Tests validation of source types by passing a supported type.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     */
    @Test
    public void testValidateSourceTypeGood() throws IOException, JSAPException {
        ArgumentParser parser = new ArgumentParser(goodSourceType);
        parser.validateType(parser.sourceType, "source");
    }

    /**
     * Tests validation of source types by passing an unsupported type.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     */
    @Test
    public void testValidateSourceTypeBad() throws IOException, JSAPException {
        String badSourceTypeString = "xxx";

        exception.expectMessage(String.format("You entered an invalid source filetype: " +
                "%s, supported source data types are: %s",
                badSourceTypeString, Arrays.toString(ArgumentParser.SOURCE_TYPES)));

        ArgumentParser parser = new ArgumentParser(badSourceType);
        parser.validateType(parser.sourceType, "source");
    }

    /**
     * Tests validation of output paths by passing a good path.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     */
    @Test
    public void testValidateOutputPathGood() throws IOException, JSAPException {
        ArgumentParser parser = new ArgumentParser(goodOutputPath);
        ArgumentParser.validateOutputPath(parser.output);
    }

    /**
     * Tests validation of output paths by passing a bad path and ensuring the path is created.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     */
    @Test
    public void testValidateOutputPathBad() throws IOException, JSAPException {
        File badOutputPathString = new File("src/test/resources/testCreateOutput").getAbsoluteFile();

        ArgumentParser parser = new ArgumentParser(badOutputPath);
        ArgumentParser.validateOutputPath(parser.output);

        //If the path exists delete it otherwise fail because it wasn't created.
        if (Files.exists(badOutputPathString.toPath())) {
            FileUtils.deleteDirectory(badOutputPathString);
        } else {
            fail("The output path was not created");
        }
    }

    /**
     * Tests validation of output types by passing a supported type.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     */
    @Test
    public void testValidateOutputTypeGood() throws IOException, JSAPException {
        ArgumentParser parser = new ArgumentParser(goodOutputType);
        parser.validateType(parser.outputType, "output");
    }

    /**
     * Tests validation of output types by passing an unsupported type.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     */
    @Test
    public void testValidateOutputTypeBad() throws IOException, JSAPException {
        String badOutputTypeString = "ggg";

        exception.expectMessage(String.format("You entered an invalid output filetype: " +
                        "%s, supported output data types are: %s",
                badOutputTypeString, Arrays.toString(ArgumentParser.OUTPUT_TYPES)));

        ArgumentParser parser = new ArgumentParser(badOutputType);
        parser.validateType(parser.outputType, "output");
    }

    /**
     * Tests that the duplicate path checker picks up duplicate paths.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     */
    @Test
    public void testCheckDuplicatePathTrue() throws IOException, JSAPException {
        ArgumentParser parser = new ArgumentParser(duplicatePath);
        parser.checkDuplicatePaths(parser.source, parser.output);
    }

    /**
     * Tests that the duplicate path checker doesn't throw false positives.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     */
    @Test
    public void testCheckDuplicatePathFalse() throws IOException, JSAPException {
        ArgumentParser parser = new ArgumentParser(goodOutputPath);
        parser.checkDuplicatePaths(parser.source, parser.output);
    }

    /**
     * Tests that the file checker finds at least one matching file.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     *
     */
    @Test
    public void testCheckForFilesTrue() throws IOException, JSAPException {
        ArgumentParser parser = new ArgumentParser(goodSourcePath);
        parser.checkForFiles(parser.source, parser.filter, parser.sourceType);
    }

    /**
     * Tests that the file checker does not throw false positives.
     * @throws java.io.IOException
     * @throws com.martiansoftware.jsap.JSAPException
     */
    @Test
    public void testCheckForFilesFalse() throws IOException, JSAPException {
        exception.expectMessage(String.format("Your specified source path" +
                " \"%s\" does not contain any xpt files", noFilesString));

        ArgumentParser parser = new ArgumentParser(noFilesPath);
        parser.checkForFiles(parser.source, parser.filter, parser.sourceType);
    }

    /**
     * Test passing in a single file and making sure it still works
     * @throws com.martiansoftware.jsap.JSAPException
     * @throws java.io.IOException
     */
    @Test
    public void testCheckForFilesSingleFile() throws JSAPException, IOException {
        ArgumentParser parser = new ArgumentParser(singleFilePath);
        parser.checkForFiles(parser.source, parser.filter, parser.sourceType);
    }

    /**
     * Tests that retrieve files finds the 3 specific files it should and nothing more.
     * @throws com.martiansoftware.jsap.JSAPException
     * @throws java.io.IOException
     */
    @Test
    public void testRetrieveFilesMultiple() throws JSAPException, IOException {
        ArgumentParser parser = new ArgumentParser(goodSourcePath);
        parser.files = parser.retrieveFiles(parser.source, parser.filter, parser.sourceType);

        //Assert it found the 3 expected files
        assertEquals(3, parser.files.size());
    }

    /**
     * Tests that retrieve files finds the specific file it should and nothing more.
     * @throws com.martiansoftware.jsap.JSAPException
     * @throws java.io.IOException
     */
    @Test
    public void testRetrieveFilesSingle() throws JSAPException, IOException {
        ArgumentParser parser = new ArgumentParser(singleFilePath);
        parser.files = parser.retrieveFiles(parser.source, parser.filter, parser.sourceType);

        //Assert it found the expected file
        assertEquals(1, parser.files.size());
    }

    /**
     *
     * @throws JSAPException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     * @throws IOException
     */
    @Test
    public void testCheckDefineArguments() throws JSAPException, SAXException,
            ParserConfigurationException, TransformerConfigurationException, IOException {

        ArgumentParser parser = new ArgumentParser(noDefineORConfigArgs);

        exception.expectMessage("You must specify either a define.xml document or" +
                " a configuration file for converting into dataset-xml." + "\n" +
                "If you need help with configuring your parameters," +
                " use the  \"--help\" command.");

        parser.checkDefineArguments();
    }
    /**
     * Checks that the validateSchema define path works properly
     * @throws JSAPException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     * @throws IOException
     */
    @Test
    public void testValidateDefinePathGood() throws JSAPException, SAXException,
            ParserConfigurationException, TransformerConfigurationException, IOException {
        ArgumentParser parser = new ArgumentParser(goodDefinePathArgs);
        parser.define = new File(definePath).getAbsoluteFile();
        parser.validatePath(parser.define, "define");
    }

    /**
     * Checks that the validateSchema define path works properly when a directory is passed
     * @throws JSAPException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     * @throws IOException
     */
    @Test
    public void testValidateDefinePathDirectory() throws JSAPException, SAXException,
            ParserConfigurationException, TransformerConfigurationException, IOException {
        ArgumentParser parser = new ArgumentParser(badDefineArgsDirectory);
        parser.define = new File(noDefinePathDirectory).getAbsoluteFile();

        exception.expectMessage(String.format("The define.xml you specified \"%s\" is not an xml document." +
                "Please check your file and try again.", parser.define.getName()));

        parser.validatePath(parser.define, "define");
    }

    /**
     * Checks that the validateSchema define path works properly when a file is passed
     * @throws JSAPException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     * @throws IOException
     */
    @Test
    public void testValidateDefinePathFile() throws JSAPException, SAXException,
            ParserConfigurationException, TransformerConfigurationException, IOException {
        ArgumentParser parser = new ArgumentParser(badDefineArgsFile);
        parser.define = new File(noDefinePathFile).getAbsoluteFile();

        exception.expectMessage(String.format("The define.xml you specified \"%s\" is not an xml document." +
                "Please check your file and try again.", parser.define.getName()));

        parser.validatePath(parser.define, "define");
    }

    /**
     * Im only testing the good scenario for the config logic because is is literally a copy of the DefinePath validation.
     * If those pass and this test passes, they will all pass.
     * @throws JSAPException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerConfigurationException
     * @throws IOException
     */
    @Test
    public void testValidateConfigPathGood() throws JSAPException, SAXException,
            ParserConfigurationException, TransformerConfigurationException, IOException {
        ArgumentParser parser = new ArgumentParser(goodConfigPathArgs);
        parser.config = new File(configPath).getAbsoluteFile();
        parser.validatePath(parser.config, "config");
    }

    /**
     * Test that a define.xml is generated at the output path if one is not supplied and a config is given.
     * @throws JSAPException
     * @throws SAXException
     * @throws IOException
     * @throws TransformerConfigurationException
     */
    @Test
    @Ignore
    public void testGenerateDefine() throws JSAPException, SAXException, IOException, TransformerConfigurationException, ParserConfigurationException {
        ArgumentParser parser = new ArgumentParser(goodConfigPathArgs);
        parser.standardName = "SDTM";
        parser.standardVersion = "3.1.3";
        parser.config = new File(configPath).getAbsoluteFile();
        parser.files = parser.retrieveFiles(parser.source, parser.filter, parser.sourceType);
        parser.generateDefine(parser.files, parser.config, parser.standardName, parser.standardVersion, parser.output);

        assertTrue(Files.exists(new File(outputPath, "/define.xml").toPath()));
    }

    @Test
    public void testValidateXPTFilesGood() throws JSAPException, IOException,
            InvalidDataException, ParserConfigurationException, SAXException {
        ArgumentParser parser = new ArgumentParser(goodDefinePathArgs);
        parser.define = new File(definePath).getAbsoluteFile();
        parser.defineXML = ArgumentParser.parseFile(parser.define);
        parser.files = parser.retrieveFiles(parser.source, parser.filter, parser.sourceType);

        for (File file : parser.files) {
            SourceOptions sourceOptions = new SourceOptions(file.getAbsolutePath());
            sourceOptions.setName(FilenameUtils.getBaseName(file.getName()));

            parser.defineXML = ArgumentParser.parseFile(parser.define);
            parser.itemGroupDefs = ArgumentParser.getItemGroupDefs(parser.defineXML);
            parser.itemGroupDefMap = ArgumentParser.getItemGroupDefMap(parser.itemGroupDefs);
            parser.itemDefMap =  ArgumentParser.getItemDefMap(parser.defineXML, parser.itemGroupDefs);
            parser.isDefine2 = ArgumentParser.getAttributes(parser.defineXML).get("Define Version").equals("2");

            DataSource source = new EnhancedSasTransportDataSource(sourceOptions);
            ArgumentParser.validateXPTFile(file, source, parser.itemGroupDefMap.keySet(),
                    parser.itemDefMap);
        }
    }

    @Test
    public void testValidateXPTFilesMissingDataset() throws JSAPException, IOException,
            InvalidDataException, ParserConfigurationException, SAXException {

        ArgumentParser parser = new ArgumentParser(badDefineMissingDatasetPathArgs);
        parser.define = new File(defineMissingDatasetPath).getAbsoluteFile();
        parser. defineXML = ArgumentParser.parseFile(parser.define);
        parser.files = parser.retrieveFiles(parser.source, parser.filter, parser.sourceType);

        for (File file : parser.files) {
            exception.expectMessage(String.format("\"%s\" was not present in your define.xml." +
                    " Try validating your define.xml against your datasets using the Validator.", FilenameUtils.getBaseName(file.getName()).toUpperCase()));

            SourceOptions sourceOptions = new SourceOptions(file.getAbsolutePath());
            sourceOptions.setName(FilenameUtils.getBaseName(file.getName()));

            parser.defineXML = ArgumentParser.parseFile(parser.define);
            parser.itemGroupDefs = ArgumentParser.getItemGroupDefs(parser.defineXML);
            parser.itemGroupDefMap = ArgumentParser.getItemGroupDefMap(parser.itemGroupDefs);
            parser.itemDefMap =  ArgumentParser.getItemDefMap(parser.defineXML, parser.itemGroupDefs);
            parser.isDefine2 = ArgumentParser.getAttributes(parser.defineXML).get("Define Version").equals("2");

            DataSource source = new EnhancedSasTransportDataSource(sourceOptions);
            ArgumentParser.validateXPTFile(file, source, parser.itemGroupDefMap.keySet(),
                    parser.itemDefMap);
        }
    }

    @Test
    public void testValidateXPTFilesMissingVariable() throws JSAPException, IOException,
            InvalidDataException, ParserConfigurationException, SAXException {

        ArgumentParser parser = new ArgumentParser(badDefineMissingVariablePathArgs);
        parser.define = new File(defineMissingVariablePath).getAbsoluteFile();
        parser.defineXML = ArgumentParser.parseFile(parser.define);
        parser.files = parser.retrieveFiles(parser.source, parser.filter, parser.sourceType);

        exception.expectMessage(String.format("One of your variables in \"ae.xpt\" was not present in your define.xml." +
                " Try validating your define.xml against your datasets using the Validator."));

        for (File file : parser.files) {
            SourceOptions sourceOptions = new SourceOptions(file.getAbsolutePath());
            sourceOptions.setName(FilenameUtils.getBaseName(file.getName()));

            parser.defineXML = ArgumentParser.parseFile(parser.define);
            parser.itemGroupDefs = ArgumentParser.getItemGroupDefs(parser.defineXML);
            parser.itemGroupDefMap = ArgumentParser.getItemGroupDefMap(parser.itemGroupDefs);
            parser.itemDefMap =  ArgumentParser.getItemDefMap(parser.defineXML, parser.itemGroupDefs);
            parser.isDefine2 = ArgumentParser.getAttributes(parser.defineXML).get("Define Version").equals("2");

            DataSource source = new EnhancedSasTransportDataSource(sourceOptions);
            ArgumentParser.validateXPTFile(file, source, parser.itemGroupDefMap.keySet(),
                    parser.itemDefMap);
        }
    }

    //TODO: Think of more test cases (Mike can probably think of some)
}
