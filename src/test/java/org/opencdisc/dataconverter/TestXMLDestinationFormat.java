/*
* Copyright © 2008-2016 Pinnacle 21 LLC
*
* This file is part of OpenCDISC Community.
*
* OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
* located at [http://www.opencdisc.org/license] (the "License").
*
* OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
* and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the License for more details.
*/

package org.opencdisc.dataconverter;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import org.opencdisc.dataconverter.destinations.XMLDestinationFormat;
import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.data.DataSource;
import org.opencdisc.validator.data.EnhancedSasTransportDataSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import util.DestinationFormatTest;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.HashMap;

import static org.junit.Assert.assertTrue;

public class TestXMLDestinationFormat extends DestinationFormatTest {
    @Test
    public void testXMLDestinationFormat() throws Exception {
        //Define.xml file to be parsed
        File define = new File("src/test/resources/defines/define.xml").getAbsoluteFile();
        //Parsed version of the define.xml
        Document defineXML;
        HashMap<String, String> itemGroupDefMap;
        HashMap<String, HashMap<String, String>> itemDefMap;
        NodeList itemGroupDefs;
        //Parsed version of the ae.xpt file
        Document aeXML;
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

        for (File file : files) {
            SourceOptions sourceOptions = new SourceOptions(file.getAbsolutePath());
            sourceOptions.setName(FilenameUtils.getBaseName(file.getName()));

            defineXML = ArgumentParser.parseFile(define);
            itemGroupDefs = ArgumentParser.getItemGroupDefs(defineXML);
            itemGroupDefMap = ArgumentParser.getItemGroupDefMap(itemGroupDefs);
            itemDefMap =  ArgumentParser.getItemDefMap(defineXML, itemGroupDefs);
            String fileName = FilenameUtils.getBaseName(file.getName()).toUpperCase();

            DataSource source = new EnhancedSasTransportDataSource(sourceOptions);
            XMLDestinationFormat xmlFormat = new XMLDestinationFormat(defineXML, outputPath.getPath(),
                    true, itemGroupDefMap.get(fileName), itemDefMap.get(fileName));
            Converter.convert(source, xmlFormat);
        }

        //The created ae.xml file
        File ae = new File("src/test/resources/testOutput/AE.xml").getAbsoluteFile();
        //Parse the xml file
        aeXML = builderFactory.newDocumentBuilder().parse(ae);
        //Get one of the itemGroupData tags
        Element itemGroupData = (Element) aeXML.getElementsByTagName("ItemGroupData").item(0);
        //Tag that should have the StudyID variable.
        Element itemData = (Element) itemGroupData.getElementsByTagName("ItemData").item(0);
        //Assert the oid is IG.AE
        assertTrue(itemGroupData.getAttribute("ItemGroupOID").equals("IG.AE"));
        //Assert that the ItemData tag has the STUDYID variable and the value is CDISC01
        assertTrue(itemData.getAttribute("ItemOID").equals("IT.STUDYID"));
        assertTrue(itemData.getAttribute("Value").equals("CDISC01"));
    }
}
