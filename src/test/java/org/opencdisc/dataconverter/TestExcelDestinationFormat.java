/*
 * Copyright © 2008-2016 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.opencdisc.dataconverter.destinations.ExcelDestinationFormat;
import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.data.DataSource;
import org.opencdisc.validator.data.EnhancedSasTransportDataSource;
import util.DestinationFormatTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

/**
 * @author Gerard
 */
public class TestExcelDestinationFormat extends DestinationFormatTest {
    /**
     * Tests that the converter correctly maps data into an excel file.
     * Specifically checks the AE.xlsx file. for arbitrary data.
     * @throws java.lang.Exception
     */
    @Test
    public void testConvertXLSX() throws Exception {
        for (File file : files) {
            SourceOptions sourceOptions = new SourceOptions(file.getAbsolutePath());
            sourceOptions.setName(FilenameUtils.getBaseName(file.getName()));
            DataSource dataSource = new EnhancedSasTransportDataSource(sourceOptions);
            ExcelDestinationFormat excelFormat = new ExcelDestinationFormat(outputPath.getPath());
            Converter.convert(dataSource, excelFormat);
        }

        //Get the excel file (only checking the AE test file)
        InputStream input = new FileInputStream(outputPath.toString() + "/AE.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook(input);

        //Check the "Dataset Metadata" sheet
        Sheet sheet = workbook.getSheetAt(0);

        Row row = sheet.getRow(1);

        assertEquals("The dataset cell did not contain \"AE\"",
                "AE", row.getCell(0).getStringCellValue());
        assertEquals("The Label row was not \"--\"",
                "--", row.getCell(1).getStringCellValue());
        assertEquals("The Variables cell did not contain \"18\"",
                "18", row.getCell(2).getStringCellValue());
        assertEquals("The Records cell did not contain \"16\"",
                "16", row.getCell(3).getStringCellValue());

        //Check the "Variable Metadata" sheet
        sheet = workbook.getSheetAt(1);

        row = sheet.getRow(5);

        assertEquals("The order number was not \"5\"",
                "5", row.getCell(0).getStringCellValue());
        assertEquals("The Variable cell did not contain \"AESPID\"",
                "AESPID", row.getCell(1).getStringCellValue());
        assertEquals("The Label cell did not contain \"Sponsor-Defined Identifier\"",
                "Sponsor-Defined Identifier", row.getCell(2).getStringCellValue());
        assertEquals("The Type cell did not contain \"Char\"",
                "Char", row.getCell(3).getStringCellValue());
        assertEquals("The Length cell did not contain \"4.0\"",
                "4.0", row.getCell(4).getStringCellValue());
        assertEquals("The Format cell was not empty",
                "", row.getCell(5).getStringCellValue());

        /*
         * Check the "AE" sheet
         * I only check one header and one cell because
         * This is all populated in one loop and
          * this small check should be sufficient.
         */
        sheet = workbook.getSheetAt(2);

        row = sheet.getRow(0);

        assertEquals("The column header was order",
                "Order", row.getCell(0).getStringCellValue());

        assertEquals("The column header was not \"STUDYID\"",
                "STUDYID", row.getCell(1).getStringCellValue());

        row = sheet.getRow(1);

        assertEquals("The Order number was 1",
                1, row.getCell(0).getNumericCellValue(), 0);

        assertEquals("The cell did not contain \"CDISC01\"",
                "CDISC01", row.getCell(1).getStringCellValue());
    }

    //TODO: Think of more test cases (Mike can probably think of some)
}
