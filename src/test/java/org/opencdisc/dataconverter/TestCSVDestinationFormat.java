/*
 * Copyright © 2008-2016 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import org.opencdisc.dataconverter.destinations.CSVDestinationFormat;
import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.data.DataSource;
import org.opencdisc.validator.data.EnhancedSasTransportDataSource;
import util.DestinationFormatTest;

import java.io.File;
import java.io.FileReader;

import static org.junit.Assert.assertArrayEquals;

/**
 * @author Gerard
 */
public class TestCSVDestinationFormat extends DestinationFormatTest {
    /**
     * Tests that the converter correctly maps data into a csv file.
     * Specifically checks the AE.csv file. for arbitrary data.
     * @throws java.io.IOException
     * @throws org.opencdisc.validator.data.InvalidDataException
     */
    @Test
    public void testConvertCSV() throws Exception {
        for (File file : files) {
            SourceOptions sourceOptions = new SourceOptions(file.getAbsolutePath());
            sourceOptions.setName(FilenameUtils.getBaseName(file.getName()));
            DataSource dataSource = new EnhancedSasTransportDataSource(sourceOptions);
            CSVDestinationFormat csvFormat = new CSVDestinationFormat(outputPath.getPath());
            Converter.convert(dataSource, csvFormat);
        }

        //Check that a row in the csv file matches the expected row (only checking AE test file).
        CSVReader reader = new CSVReader(new FileReader(outputPath + "/AE.csv"));

        //The expected header row
        String[] header = {"Order", "STUDYID", "DOMAIN", "USUBJID", "AESEQ", "AESPID", "AETERM", "" +
                "AEMODIFY", "AEDECOD", "AEBODSYS", "AESEV", "AESER", "AEACN", "AEREL", "AESTDTC", "" +
                "AEENDTC", "AESTDY", "AEENDY", "AEENRF"};

        //The expected line of data under the header
        String[] line1 = {"1" , "CDISC01", "AE", "CDISC01.100008", "1", "1", "" +
                "AGITATED", "AGITATION", "Agitation", "Psychiatric disorders", "" +
                "MILD", "N", "DOSE NOT CHANGED", "POSSIBLY RELATED", "2003-05", "", "3", "", "AFTER"};

        assertArrayEquals("The header row does not match",
                header, reader.readNext());

        assertArrayEquals("The data row does not match",
                line1, reader.readNext());

        reader.close();
    }

    //TODO: Think of more test cases (Mike can probably think of some)
}
