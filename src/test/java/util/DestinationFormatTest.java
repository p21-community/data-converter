/*
 * Copyright © 2008-2016 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package util;

import org.junit.Before;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;

public abstract class DestinationFormatTest {
    /**
     * This is always the output path regardless of file integrity.
     */
    protected File outputPath = new File("src/test/resources/testOutput");

    /**
     * Collection of test files
     */
    protected Collection<File> files = new ArrayList<>();

    /**
     * Delete any files in the test directory that may be found
     * Loads the collection with test files
     *
     * @throws java.io.IOException
     */
    @Before
    public void setFiles() throws IOException {
        if (!outputPath.exists()) {
            Files.createDirectory(outputPath.toPath());
        }

        files.add(new File("src/test/resources/ae.xpt").getAbsoluteFile());
        files.add(new File("src/test/resources/split/qscg.xpt").getAbsoluteFile());
        files.add(new File("src/test/resources/split/qscs.xpt").getAbsoluteFile());

        outputPath = outputPath.getAbsoluteFile();
    }
}
