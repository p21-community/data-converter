/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter;

import java.net.URL;
import org.apache.poi.Version;
import org.opencdisc.ipc.Bridge;

public class Main {
    /**
     * Calls {@link ArgumentParser#parse()}.
     * @param args The full path to the source data files
     *             Extension of source data files,
     *             The full path to place the converted data files
     *             Desired extension of converted data files
     */
    public static void main(String[] args) {
        if (Bridge.isBridged(args)) {
            Connector.attach();
        } else {
            try {
                ArgumentParser parser = new ArgumentParser(args);
                parser.parse();
            } catch (Throwable ex) {
                ClassLoader classloader = Version.class.getClassLoader();
                URL resource = classloader.getResource(Version.class.getName().replace('.', '/') + ".class");

                if (resource != null) {
                    System.out.println("POI Core came from " + resource.getPath());
                }

                System.out.println("An unexpected exception has occurred:");
                ex.printStackTrace();
            }
        }
    }
}
