/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.opencdisc.dataconverter.destinations.DestinationFormat;
import org.opencdisc.dataconverter.destinations.VariableMetadata;
import org.opencdisc.validator.data.DataRecord;
import org.opencdisc.validator.data.DataSource;
import org.opencdisc.validator.model.EntityDetails;

import java.util.Set;

public class Converter {

    public Converter() {}

    private static final Logger logger = LogManager.getLogger(Converter.class);

    /**
     * Converts a <code>DataSource</code> into a <code>DestinationFormat</code>.
     * @param source the DataSource
     * @param format the DestinationFormat
     * @throws Exception because of the <code>AutoCloseable</code> interface.
     */
    public static void convert(DataSource source, DestinationFormat format) throws Exception {
        logger.debug("In Convert Method");

        Set<String> variables = source.getVariables();
        EntityDetails details = source.getDetails();

        //Create the file from the source
        format.setName(details.hasProperty(EntityDetails.Property.Subname) ?
                details.getString(EntityDetails.Property.Subname) :
                details.getString(EntityDetails.Property.Name));

        if (details.hasProperty(EntityDetails.Property.Label)) {
            format.setLabel(details.getString(EntityDetails.Property.Label));
        }

        format.setPath(details.getString(EntityDetails.Property.Location));

        logger.debug("Number of Variables: " + variables.size());

        for (String variable : variables) {
            //Create the variables from the data source
            format.setVariable(new VariableMetadata(
                variable,
                (String) source.getVariableProperty(variable, DataSource.VariableProperty.Label),
                (String) source.getVariableProperty(variable, DataSource.VariableProperty.Type),
                (Double) source.getVariableProperty(variable, DataSource.VariableProperty.Length),
                (String) source.getVariableProperty(variable, DataSource.VariableProperty.Format)
            ));
        }

        logger.debug("Retrieved variables");
        logger.debug("Source has Records: " + source.hasRecords());
        logger.debug("Location: " + source.getLocation());
        logger.debug("Record Count: " + source.getRecordCount());

        //Write the records
        while (source.hasRecords() && !format.isTruncated()) {
            logger.debug("WRITING START");

            for (DataRecord record : source.getRecords()) {
                if (!format.writeRecord(record)) {
                    logger.debug("BROKEN");
                    break;
                }
            }
        }

        logger.debug("Wrote Records");
        //Close the destination format
        format.close();
    }
}
