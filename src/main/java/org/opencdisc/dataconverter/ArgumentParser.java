/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter;

import com.martiansoftware.jsap.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.opencdisc.dataconverter.destinations.CSVDestinationFormat;
import org.opencdisc.dataconverter.destinations.ExcelDestinationFormat;
import org.opencdisc.dataconverter.destinations.XMLDestinationFormat;
import org.opencdisc.define.DefineGenerator;
import org.opencdisc.define.SASTransportParser;
import org.opencdisc.define.models.Study;
import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.data.DataSource;
import org.opencdisc.validator.data.EnhancedSasTransportDataSource;
import org.opencdisc.validator.data.InvalidDataException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class ArgumentParser {
    /** The allowed source file extensions. */
    public static final String[] SOURCE_TYPES = {"xpt"};
    /** The allowed output file extensions. */
    public static final String[] OUTPUT_TYPES = {"xlsx", "csv", "xml"};
    /** The file filter for getting the files with a matching source type. */
    protected SuffixFileFilter filter;
    /** The parser used for getting command line arguments. */
    protected SimpleJSAP jsap;
    /** Result from parsing the arguments. */
    protected JSAPResult result;
    /** Source file path. */
    protected File source;
    /** Output file path. */
    protected File output;
    /** Source filetype. */
    protected String sourceType;
    /** Output filetype. */
    protected String outputType;
    /** Path to a define.xml for use in xml conversion. */
    protected File define;
    /** The define.xml after being loaded into DOM for use in dataset-xml conversions. */
    protected Document defineXML;
    /** Config for generating define.xml files. */
    protected File config;
    /**The parsed config.xml document for use in dataset-xml conversions. */
    protected Document configXML;
    /** List of ItemGroupDefs in the define.xml. */
    protected NodeList itemGroupDefs;
    /** Map between the ItemGroupDef name and the OID */
    protected HashMap<String, String> itemGroupDefMap;
    /** Complete Map between each ItemGroupDef's name then a map between each variable name and the OID from the ItemGroupDef */
    protected HashMap<String, HashMap<String, String>> itemDefMap;
    /** The file(s) given to be converted. */
    protected Collection<File> files;
    /** Map of the standard name, standard version and Define Version. */
    protected HashMap<String, String> attributes;
    /** For use in generating a define.xml */
    protected String standardName;
    /** For use in generating a define.xml */
    protected String standardVersion;
    /** If the supplied define.xml document is version 2 or 1 */
    protected boolean isDefine2;
    /** If the Dataset-XML validation fails, this will be true. */
    protected static boolean aborted = false;
    /** If --help is supplied as an argument, this will be true. */
    protected boolean help = false;

    /**
     * Construct the argument parser by getting the arguments from the command line.
     * @param args the command line arguments.
     * @throws JSAPException
     */
    public ArgumentParser(String[] args) throws JSAPException {
        this.help = Arrays.asList(args).contains("--help");
        this.jsap = new SimpleJSAP(
                "java -jar " + new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getName(),
                "--------------------------------------------------------------------------\n" +
                "\n" +
                "\t\tOpenCDISC Data Converter Command Line Client\n" +
                "\t\t\t\t  \n" +
                "--------------------------------------------------------------------------\n" +
                "\n" +
                "The following parameters may be passed to the Data Converter. Note that\n" +
                "the source and output path are required. Source type is defaulted to xpt\n" +
                "and output type is defaulted to xlsx.\n" +
                "\n" +
                "For additional help, or to submit suggestions, please visit our community\n" +
                "at http://www.opencdisc.org/",
                new Parameter[] {
                        new FlaggedOption("source", JSAP.STRING_PARSER, null, JSAP.REQUIRED, 's', JSAP.NO_LONGFLAG,
                                "   Full path to source data files."),
                        new FlaggedOption("sourceType", JSAP.STRING_PARSER, "xpt", JSAP.REQUIRED, 'i', JSAP.NO_LONGFLAG,
                                String.format("Data type of source files." +
                                        " Currently supported file types are %s", Arrays.toString(SOURCE_TYPES))),
                        new FlaggedOption("output", JSAP.STRING_PARSER, null, JSAP.REQUIRED, 'o', JSAP.NO_LONGFLAG,
                                "Full path to place output data files"),
                        new FlaggedOption("outputType", JSAP.STRING_PARSER, "xlsx", JSAP.REQUIRED, 'e', JSAP.NO_LONGFLAG,
                                String.format("Desired data type of output files." +
                                        " Currently supported file types are %s", Arrays.toString(OUTPUT_TYPES))),
                        new FlaggedOption("define", JSAP.STRING_PARSER, null, JSAP.NOT_REQUIRED, 'd', JSAP.NO_LONGFLAG,
                                "For use in converting to Dataset-XML. Path to the define.xml for your datasets." +
                                        " If you do not have a define.xml you must" +
                                        " specify the path to a configuration file so one can be generated."),
                        new FlaggedOption("config", JSAP.STRING_PARSER, null, JSAP.NOT_REQUIRED, 'c', JSAP.NO_LONGFLAG,
                                "For use in converting to Dataset-XML. Path to the configuration xml" +
                                        " file for generating a define.xml. An incomplete define.xml" +
                                        " will be generated based off of this configuration file." +
                                        " The configuration files are packaged with OpenCDISC Community.")
                }
        );

        //Parse the args using jsap
        this.result = jsap.parse(args);

        //If the arguments were not parsed successfully
        if (!result.success()) {
            StringBuilder errors = new StringBuilder();

            //TODO figure out why this generates duplicate messages when no arguments are passed.
            //Load JSAP's error messages.
            for (java.util.Iterator messages = this.result.getErrorMessageIterator();
                 messages.hasNext();) {
                errors.append("Error: ").append(messages.next()).append("\n");
            }

            errors.append("If you need help with configuring your parameters," +
                    " use the  \"--help\" command.");

            if (!help) {
                throw new IllegalArgumentException(errors.toString());
            }
        } else {
            //otherwise assign the properties
            this.sourceType = this.result.getString("sourceType").trim().toLowerCase();
            this.outputType = this.result.getString("outputType").trim().toLowerCase();
            this.source = new File(this.result.getString("source").trim());
            this.output = new File(this.result.getString("output").trim());
            //Generate the filter for getting files
            this.filter = createFileFilter(this.sourceType);
        }
    }

    /**
     * Validate the parameters and get the file(s).
     * @throws IOException
     */
    public void parse() throws Exception {
        if (help) { return; }

        SourceOptions sourceOptions;
        DataSource source;
        validateTypes(this.sourceType, this.outputType);
        validatePaths(this.source, this.output);
        this.files = retrieveFiles(this.source, this.filter, this.sourceType);

        //If we are converting to dataset-xml
        if (this.outputType.equals("xml")) {
            validateXMLArguments();
            this.itemGroupDefs = getItemGroupDefs(this.defineXML);
            this.itemGroupDefMap = getItemGroupDefMap(this.itemGroupDefs);
            this.itemDefMap = getItemDefMap(this.defineXML, this.itemGroupDefs);

            for (File file : this.files) {
                sourceOptions = new SourceOptions(file.getAbsolutePath());
                sourceOptions.setName(FilenameUtils.getBaseName(file.getName()));
                source = new EnhancedSasTransportDataSource(sourceOptions);
                validateXPTFile(file, source, itemGroupDefMap.keySet(), itemDefMap);
            }
        }

        //Call the correct converter according to output type since the only source currently is XPT
        if (!aborted) {
            for (File file : this.files) {
                sourceOptions = new SourceOptions(file.getAbsolutePath());
                sourceOptions.setName(FilenameUtils.getBaseName(file.getName()));
                source = new EnhancedSasTransportDataSource(sourceOptions);
                switch (this.outputType) {
                    case "xlsx":
                        ExcelDestinationFormat excel = new ExcelDestinationFormat(this.output.getPath());
                        Converter.convert(source, excel);
                        break;
                    case "csv":
                        CSVDestinationFormat csv = new CSVDestinationFormat(this.output.getPath());
                        Converter.convert(source, csv);
                        break;
                    case "xml":
                        String fileName = FilenameUtils.getBaseName(file.getName()).toUpperCase();
                        XMLDestinationFormat xml = new XMLDestinationFormat(this.defineXML, this.output.getPath(),
                                this.isDefine2, itemGroupDefMap.get(fileName), itemDefMap.get(fileName));
                        Converter.convert(source, xml);
                    default:
                }
            }
        }
    }

    /**
     * Aggregate to validateSchema source and output data types.
     * @throws java.io.IOException
     */
    public void validateTypes(String sourceType, String outputType) throws IOException {
        checkDuplicateTypes(sourceType, outputType);
        validateType(sourceType, "source");
        validateType(outputType, "output");
    }

    /**
     * Aggregate to validateSchema source and output paths.
     * @throws java.io.IOException
     */
    public void validatePaths(File source, File output) throws IOException {
        checkDuplicatePaths(source, output);
        validateSourcePath(source);
        validateOutputPath(output);
    }

    /**
     * Aggregate to validateSchema all arguments required for dataset-xml conversion.
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerConfigurationException
     * @throws InvalidDataException
     */
    public void validateXMLArguments() throws IOException, ParserConfigurationException,
            SAXException, TransformerConfigurationException, InvalidDataException {
        checkDefineArguments();
    }

    /**
     * Verify that the source exists on the computer.
     */
    public void validateSourcePath(File source) throws FileNotFoundException {
        //Convert source path to an absolute path if it is relative.
        if (!source.isAbsolute()) {
            source = source.getAbsoluteFile();
        }

        //Verify that the source path exists on the computer.
        if (Files.notExists(source.toPath())) {
            throw new FileNotFoundException(String.format("Your specified source path \"%s\" does not exist. " +
                    "Please verify the path and try again.", source.toString()));
        }
    }

    /**
     * Verify that the output path exists on the computer.
     * Create it if it does not exist.
     */
    public static void validateOutputPath(File output) throws IOException {
        //Convert output path to an absolute path if it is relative.
        if (!output.isAbsolute()) {
            output = output.getAbsoluteFile();
        }

        //Verify that the output path exists on the computer (create it if it doesn't)
        if (Files.notExists(output.toPath())) {
            Files.createDirectory(output.toPath());
        }
    }

    /**
     * Verify that the source path and output path are not the same.
     * @throws java.io.IOException
     */
    public void checkDuplicatePaths(File source, File output) throws IOException {
        if (source.equals(output)) {
            throw new IOException("The source path and the output path are the same, " +
                    "try creating a sub-folder like %source%/converted.");
        }
    }

    /**
     * Verify that the file data type is valid.
     *
     * @param fileType the file extension
     * @param type "source" for source file type or "output" for output file type
     *
     * @throws java.io.IOException if the data type is invalid.
     */
    public void validateType(String fileType, String type) throws IOException {
        if (!Arrays.asList(type.equals("source") ? SOURCE_TYPES : OUTPUT_TYPES).contains(fileType)) {
            throw new IOException(String.format("You entered an invalid %s filetype: %s, " +
                    "supported %s data types are: "
                    + (Arrays.toString(type.equals("source") ? SOURCE_TYPES : OUTPUT_TYPES)), type, fileType, type));
        }
    }

    /**
     * Verify that the two data types are not the same.
     *
     * @param sourceType the source file extension
     * @param outputType the desired output file extension
     *
     * @throws java.io.IOException if the data types are the same
     */
    public void checkDuplicateTypes(String sourceType, String outputType) throws IOException {
        if (sourceType.equals(outputType)) {
            throw new IOException("Your source and output data types are the same," +
                    "there is no need to convert your files.");
        }
    }

    /**
     * Verifies that the source path contains at least one of the specified files.
     * It will also look inside any folder named "split" for split datasets.
     *
     * @throws java.io.IOException
     */
    public String checkForFiles(File source, SuffixFileFilter filter, String sourceType) throws IOException {
        String type;

        //If they specified a directory
        if (source.isDirectory()) {
            if (FileUtils.listFiles(source, filter, FileFilterUtils.nameFileFilter("split")).size() < 1) {
                throw new IOException(String.format("Your specified source path \"%s\" does not contain any %s files."
                        , source.toString(), sourceType));
            }
            type = "Collection";
        //Otherwise it better be a file
        } else if (!FilenameUtils.getExtension(source.getName()).equalsIgnoreCase(sourceType)) {
                throw new IOException(String.format("Your specified source file \"%s\" is not a %s file."
                        , source.toString(), sourceType));

        } else {
            type = "File";
        }

        return type;
    }

    /**
     * Get file(s) with a specified type within the specified source directory.
     * It will also look inside any folder named "split" for split datasets.
     * @throws java.io.IOException
     */
    public Collection<File> retrieveFiles(File source, SuffixFileFilter filter, String sourceType) throws IOException {
        //Make sure file(s) are there
        String type = checkForFiles(source, filter, sourceType);

        Collection<File> fileCollection = new ArrayList<>();

        if (type.equals("Collection")) {
           fileCollection = FileUtils.listFiles(source, filter, FileFilterUtils.nameFileFilter("split"));
        } else if (type.equals("File")) {
            fileCollection = new ArrayList<>();
            fileCollection.add(source);
        }

        return fileCollection;
    }

    /**
     * Create the file filter for getting the source file(s).
     */
    public SuffixFileFilter createFileFilter(String sourceType) {
        return new SuffixFileFilter(sourceType, IOCase.INSENSITIVE);
    }

    /**
     * If the user specifies a define.xml it will validateSchema it.
     * If the user specifies a config it will validateSchema that instead.
     * If the user specifies neither it will stop the process.
     * @throws IOException
     * @throws TransformerConfigurationException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public void checkDefineArguments() throws IOException, TransformerConfigurationException,
            SAXException, ParserConfigurationException {
        //If the user specifies a define.xml path
        if (this.result.contains("define")) {
            this.define = new File(this.result.getString("define"));
            validatePath(define, "define");
            this.defineXML = parseFile(this.define);
            this.attributes = getAttributes(this.defineXML);

            this.standardName = this.attributes.get("Standard Name");
            this.standardVersion = this.attributes.get("Standard Name");
            this.isDefine2 = this.attributes.get("Define Version").equals("2");

          //If the user specifies a configuration file
        } else if (this.result.contains("config")) {
            this.config = new File(this.result.getString("config"));
            validatePath(config, "config");
            this.configXML = parseFile(this.config);
            this.attributes = getAttributes(this.configXML);

            this.standardName = this.attributes.get("Standard Name");
            this.standardVersion = this.attributes.get("Standard Name");
            this.isDefine2 = this.attributes.get("Define Version").equals("2");

            parseFile(generateDefine(this.files, this.config, this.standardName, this.standardVersion, this.output));
        } else {
            //Otherwise they did something wrong.
            throw new IllegalArgumentException("You must specify either a define.xml document or" +
                    " a configuration file for converting into dataset-xml." + "\n" +
                    "If you need help with configuring your parameters," +
                    " use the  \"--help\" command.");
        }
    }

    /**
     * Checks to see if the specified argument was actually an xml document.
     * Will fail if the argument was a directory or not an xml document.
     *
     * @param file the file to check
     * @param type either "define" for a define.xml or "config" for a configuration file.
     *
     * @throws java.io.IOException
     */
    public void validatePath(File file, String type) throws IOException {
        //Check that the define path is an absolute path.
        if (!file.isAbsolute()) {
            file = file.getAbsoluteFile();
        }

        if (!file.isDirectory()) {
            if (!FilenameUtils.getExtension(file.getName()).equalsIgnoreCase("xml")) {
                throw new IOException(String.format("The %s.xml you specified \"%s\" is not an xml document." +
                        "Please check your file and try again.", type, file.getName()));
            }
        } else {
            throw new IOException(String.format("The %s.xml you specified \"%s\" is not an xml document." +
                    "Please check your file and try again.", type, file.getName()));
        }
    }

    /**
     * Generate a define.xml and set the path of the document.
     * Saves the document in the location where the files will be placed.
     *
     * @param files the collection of files to be included in your define document.
     * @param config the standard you will use for your define
     * @param standardName the name of your standard
     * @param standardVersion  the version of your standard
     * @param output where to save your define, it should be where you're converting the files
     *
     * @return the define.xml
     *
     * @throws IOException
     * @throws SAXException
     * @throws TransformerConfigurationException
     */
    public File generateDefine(Collection<File> files, File config,
                               String standardName, String standardVersion, File output)
            throws IOException, SAXException, TransformerConfigurationException, ParserConfigurationException {
        //Sanity check to make sure the output exists, because the generator doesn't for some reason
        if (!output.exists()) {
            Files.createDirectory(output.toPath());
        }

        SASTransportParser sasTransportParser = new SASTransportParser(files, config, new Study()
                .setName("[Study]")
                .setDescription("[StudyDescription]")
                .setProtocol("[StudyProtocol]")
                .setStandardName(standardName)
                .setStandardVersion(standardVersion)
                .setLanguage("en"));
        Study study = sasTransportParser.parse();
        DefineGenerator generator = new DefineGenerator(study, new File(output.getAbsolutePath()));
        generator.generate();

        return new File(output, "define.xml").getAbsoluteFile();
    }

    /**
     * Load the define.xml or config.xml into memory.
     * This will actually parse any file you want into memory.
     *
     * @param file the file you want parsed into a document
     * @return the parsed document
     *
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public static Document parseFile(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        return builderFactory.newDocumentBuilder().parse(file);
    }

    /**
     * Gets the standard name, version and define version from a document.
     * @param document either the config or define document
     * @return a map of the StandardName, StandardVersion and DefineVersion
     */
    public static HashMap<String, String> getAttributes(Document document) {
        HashMap<String, String> attributes = new HashMap<>();

        NodeList metaDataVersionList = document.getElementsByTagName("MetaDataVersion");
        Element  metaDataVersion = (Element) metaDataVersionList.item(0);
        attributes.put("Standard Name", metaDataVersion.getAttribute("def:StandardName"));
        attributes.put("Standard Version", metaDataVersion.getAttribute("def:StandardVersion"));
        attributes.put("Define Version", metaDataVersion.getAttribute("def:DefineVersion").contains("2") ? "2" : "1");

        return attributes;
    }

    /**
     * Check that the xpt file is contained within the define.xml.
     * Also check that all of the variables are inside define.xml
     * @param file
     * @param source
     * @throws org.opencdisc.validator.data.InvalidDataException
     * @throws java.io.IOException
     *
     */
    public static void validateXPTFile(File file, DataSource source, Set<String> itemGroupDefs,
                                       HashMap<String, HashMap<String, String>> itemDefMap)
            throws InvalidDataException, IOException {
        String fileName = FilenameUtils.getBaseName(file.getName()).trim().toUpperCase();

        //If the file is not contained in the list of ItemGroupDefs.
        if (!itemGroupDefs.contains(fileName)) {
            aborted = true;
            throw new IOException(String.format("\"%s\" was not present in your define.xml." +
                    " Try validating your define.xml against your datasets using the Validator.", fileName));
        }

        Set<String> itemDefs = itemDefMap.get(fileName).keySet();

        //For every variable in the file
        for (String variable : source.getVariables()) {
            //Check if the define contains it
            if (!itemDefs.contains(variable.toUpperCase())) {
                aborted = true;
                throw new IOException(String.format("One of your variables in \"%s\" was not present in your define.xml." +
                        " Try validating your define.xml against your datasets using the Validator.", file.getName()));
            }
        }
    }

    /**
     * Gets the ItemGroupDefs from a define.xml.
     * @param defineXML the define which has your ItemGroupDefs
     * @return the NodeList of ItemGroupDefs
     */
    public static NodeList getItemGroupDefs(Document defineXML) {
        return defineXML.getElementsByTagName("ItemGroupDef");
    }

    /**
     * <p>Builds a map between an ItemGroupDef name and its OID from the define.xml.</p>
     *
     * @param itemGroupDefs list of itemGroupDefs from the define.xml
     * @return the complete mapping between an ItemGroupDef's name and OID
     * @since <a href="https://pinnacle21.atlassian.net/browse/OPEN-186">OPEN-186</a>
     */
    public static HashMap<String, String> getItemGroupDefMap(NodeList itemGroupDefs) {
        HashMap<String, String> completeMap = new HashMap<>();

        for (int i = 0; i < itemGroupDefs.getLength(); i++) {
            //Get the next ItemGroupDef
            Element element = ((Element) itemGroupDefs.item(i));

            //Get the full variable OID
            String oid = element.getAttribute("OID");
            //Strip the preceding Junk like IT. or AE. off of the variable oid
            String[] elementParts = oid.split("\\.");
            //Get only the variable name, which will be the last index.
            String name = elementParts[elementParts.length - 1];

            completeMap.put(name.toUpperCase(), oid.toUpperCase());
        }
        return completeMap;
    }

    /**
     * <p>Builds a comprehensive list of almost everything needed for Dataset-XML validation.</p>
     *
     * <p>The first item in the map is the ItemGroupDef name.
     * Each ItemGroupDef's map contains the variable name and then the full OID of the variable.
     * This is used exclusively in Dataset-XML conversions.</p>
     *
     * @param defineXML the define.xml for which you want the map
     * @param itemGroupDefs the list of itemGroupDefs from the define.xml
     * @return a complete list of mappings between ItemGroupDefs and its ItemRef's names and OIDS.
     * @since <a href="https://pinnacle21.atlassian.net/browse/OPEN-186">OPEN-186</a>
     */
    public static HashMap<String, HashMap<String, String>> getItemDefMap(Document defineXML, NodeList itemGroupDefs) {
        String itemGroupDefName;
        NodeList itemRefList;
        HashMap<String, HashMap<String, String>> completeList = new HashMap<>();
        HashMap<String, String> variableMap;
        Element element;

        if(itemGroupDefs == null) {
            itemGroupDefs = getItemGroupDefs(defineXML);
        }

        //For every ItemGroupDef in the define.
        for (int i = 0; i < itemGroupDefs.getLength(); i++) {
            //Get the next ItemGroupDef
            element = ((Element) itemGroupDefs.item(i));
            //Save the ItemGroupDef's name
            itemGroupDefName = element.getAttribute("Name");
            //Get the NodeList of ItemRefs for that ItemGroupDef
            itemRefList = element.getElementsByTagName("ItemRef");
            //Clear the previous map.
            variableMap = new HashMap<>();

            //Then for all the Nodes in the itemRefList
            for(int j = 0; j < itemRefList.getLength(); j++) {
                //Get the next ItemRef
                element = ((Element) itemRefList.item(j));
                //Get the full variable OID
                String oid = element.getAttribute("ItemOID");
                //Strip the preceding Junk like IT. or AE. off of the variable oid
                String[] elementParts = oid.split("\\.");
                //Get only the variable name, which will be the last index.
                String name = elementParts[elementParts.length - 1];

                variableMap.put(name.toUpperCase(), oid.toUpperCase());
            }
            //Add the set to the complete list.
            completeList.put(itemGroupDefName.toUpperCase(), variableMap);
        }
        return completeList;
    }
}
