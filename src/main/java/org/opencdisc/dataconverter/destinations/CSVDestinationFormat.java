/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter.destinations;

import au.com.bytecode.opencsv.CSVWriter;
import org.opencdisc.validator.data.DataRecord;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of <code>AbstractDestinationFormat</code> for use in writing csv files.
 *
 */
public class CSVDestinationFormat extends AbstractDestinationFormat {

    /** The csv file used in CSV conversion */
    private File csvFile;

    /** The csv writer used in the conversion */
    private CSVWriter writer;

    /** Value used to print the order column so users can revert to the original order. */
    private int order = 1;

    /**
     * Set the output path for the converted file.
     *
     * @param outputPath the path to save the file.
     */
    public CSVDestinationFormat(String outputPath) {
        super(outputPath);
    }


    @Override
    public boolean writeRecord(DataRecord record) {
        if (this.csvFile == null) {
            this.csvFile = new File(outputPath, this.name + ".csv");
            //Try to construct a writer with the specified file and the separator will be a comma.
            try {
                this.writer =  new CSVWriter(new FileWriter(this.csvFile), ',');
            } catch (IOException e) {
                e.getMessage();
                e.printStackTrace();
            }
            writeHeaderRow();
        }

        //Write the entire file
        //List of each record as a string array
        List<String[]> records = new ArrayList<>();

        String[] recordRow = new String[variableMetadata.size()+ 1];
        //Build a row in the file
        recordRow[0] = String.valueOf(order);
        for (int i = 0; i < variableMetadata.size(); i++) {
            recordRow[i + 1] = record.getValue(variableMetadata.get(i).name).toString();
        }
        records.add(recordRow);
        writer.writeAll(records);
        order++;

        return true;
    }

    @Override
    public void close() throws Exception {
        writer.close();
    }

    /**
     * Writes the header row of the csv file
     */
    private void writeHeaderRow() {
        String[] headerRow = new String[variableMetadata.size() + 1];
        headerRow[0] = "Order";
        for (int i = 0; i < variableMetadata.size(); i++) {
            headerRow[i + 1] = variableMetadata.get(i).name;
        }
        writer.writeNext(headerRow);
    }
}


