/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter.destinations;

import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of <code>Destinationformat</code>.
 *
 * Should be extended for specific use cases as it
 * does not contain any specific data about a data type.
 *
 */
public abstract class AbstractDestinationFormat implements DestinationFormat {

    /** Name of the file. */
    protected String name;
    /** Label of the file (if it has one). */
    protected String label;
    /** Source path of the file. */
    protected String path;
    /** Path to place the file. */
    protected String outputPath;
    /** All of the metadata for each variable. */
    protected final List<VariableMetadata> variableMetadata = new LinkedList<>();

    /** Flag if the file is truncated, set to true if the file is truncated */
    protected boolean truncated = false;

    /**
     * Set the output path for the converted file.
     * @param outputPath the path to save the file.
     */
    public AbstractDestinationFormat(String outputPath) {
        this.outputPath = outputPath;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;

    }

    @Override
    public void setVariable(VariableMetadata variableMetadata) {
        this.variableMetadata.add(variableMetadata);
    }

    @Override
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean isTruncated() {
        return this.truncated;
    }
}
