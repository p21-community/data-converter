/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter.destinations;

import org.opencdisc.validator.data.DataRecord;

import java.io.IOException;

/**
 * Interface for writing XPT files into any format.
 *
 */
public interface DestinationFormat extends AutoCloseable {

    public void setName(String name);

    public void setLabel(String label);

    public void setVariable(VariableMetadata variableMetadata);

    public void setPath(String path);

    public boolean writeRecord(DataRecord record) throws IOException;

    public boolean isTruncated();

}
