/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter.destinations;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.opencdisc.validator.data.DataRecord;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Implementation of <code>AbstractDestinationFormat</code> for use in writing excel files.
 *
 */
public class ExcelDestinationFormat extends AbstractDestinationFormat {

    /** Set the allowed row count to -1 to allow manual control of row flushing */
    private static final int ROW_ACCESS_SIZE = -1;

    /** Workbook used in Excel conversion. */
    private SXSSFWorkbook workBook;

    /** Cell Styles */
    private XSSFCellStyle styleOddRow;
    private XSSFCellStyle styleEvenRow;
    private XSSFCellStyle styleHeader;

    /** Number of records from the data source. */
    private int recordNumber;

    /** Value used to print the order column so users can revert to the original order. */
    private int order = 1;

    /**
     * Set the output path.
     * @param outputPath location to save the file.
     */
    public ExcelDestinationFormat(String outputPath) {
        super(outputPath);
    }

    @Override
    public boolean writeRecord(DataRecord record) {
       if (this.workBook == null) {
            this.workBook = new SXSSFWorkbook(ROW_ACCESS_SIZE);
           createStyle();
           createDataSheet();
       }

        //Start at the row under the headers
        int rowNumber = record.getID();
        int columnNumber;
        Cell cell;
        Sheet sheet = workBook.getSheet(name);
        columnNumber = 0;

        Row row = sheet.createRow(rowNumber);
        //Write the order number
        cell = row.createCell(columnNumber);
        //Write the order value
        cell.setCellValue(order);
        cell.setCellStyle(rowNumber % 2 > 0 ? this.styleOddRow : this.styleEvenRow);
        order++;
        columnNumber++;

        for (VariableMetadata variable : this.variableMetadata) {
            cell = row.createCell(columnNumber);
            cell.setCellValue(record.getValue(variable.name).toString());
            cell.setCellStyle(rowNumber % 2 > 0 ? this.styleOddRow : this.styleEvenRow);
            sheet.setColumnWidth(columnNumber, 3750);
            columnNumber++;
        }

        //Flush 1000 rows to disc
        if (rowNumber % 1000 == 0) {
            try {
                ((SXSSFSheet) sheet).flushRows();
            } catch (IOException io) {
                System.err.println(io.getMessage());
            }

        }

        //Check if the file has reached the excel row limit;
        if (rowNumber == 1048575) {
            this.truncated = true;
            return false;
        }

        //Count the records
        recordNumber++;
        return true;
    }

    /**
     * Create the 2 metadata sheets and order the sheets.
     * Save the file.
     * @throws java.io.IOException
     */
    @Override
    public void close() throws IOException, TruncationException {
        createDatasetMetadataSheet();
        createVariableMetadataSheet();

        this.workBook.setSheetOrder("Dataset Metadata", 0);
        this.workBook.setActiveSheet(0);
        this.workBook.setSelectedTab(0);
        this.workBook.setSheetOrder("Variable Metadata", 1);
        this.workBook.setSheetOrder(this.name, 2);

        if (truncated) {
            save();
            throw new TruncationException(String.format("The file %s was truncated to 1,048,575 records" +
                   " because the XPT file was too large.", this.name + ".xlsx"), this.name);
        } else {
            save();
        }
    }

    /**
     * Dynamically create sheet headers and populate the cells.
     */
    private void createDataSheet() {
        int rowNumber = 0;
        int columnNumber = 0;
        Cell cell;

        Sheet sheet = this.workBook.createSheet(this.name);

        //Create header row
        Row headerRow = sheet.createRow(rowNumber);
        //Create the Order column
        cell = headerRow.createCell(columnNumber);
        cell.setCellValue("Order");
        cell.setCellStyle(styleHeader);
        columnNumber++;
        //Create the rest
        for (VariableMetadata variable : this.variableMetadata) {
            cell = headerRow.createCell(columnNumber);
            cell.setCellValue(variable.name);
            cell.setCellStyle(styleHeader);
            columnNumber++;
        }
        //Freeze the header row
        sheet.createFreezePane(0, 1, 0, 1);
    }

    /**
     * Create the sheet containing information about the file.
     */
    private void createDatasetMetadataSheet() {
        Map<String, Object> metadata = new LinkedHashMap<>();

        metadata.put("Dataset", this.name);
        metadata.put("Label", this.label);
        metadata.put("Variables", this.variableMetadata.size());
        metadata.put("Records", this.recordNumber);

        Sheet sheet = this.workBook.createSheet("Dataset Metadata");
        Row header = sheet.createRow(0);
        Row data = sheet.createRow(1);
        Cell cell;
        Row row;

        int columnNumber = 0;

        //Get the file metadata from the map
        for (Map.Entry<String, Object> column : metadata.entrySet()) {
            //Create a header cell
            cell = header.createCell(columnNumber);
            //Write the header value
            cell.setCellValue(column.getKey());
            cell.setCellStyle(styleHeader);

            //Create the cell to hold the data
            cell = data.createCell(columnNumber);

            //If there is a value, populate it otherwise put "--"
            try {
                cell.setCellValue(column.getValue().toString());
                cell.setCellStyle(styleEvenRow);
            } catch (NullPointerException e) {
                cell.setCellValue("--");
                cell.setCellStyle(styleEvenRow);
            }
            columnNumber++;
        }

        //Create the source row
        row = sheet.createRow(3);
        cell = row.createCell(0);
        cell.setCellValue("Source");
        cell.setCellStyle(styleHeader);

        //Insert the source path
        cell = row.createCell(1);
        cell.setCellValue(this.path);
        cell.setCellStyle(styleEvenRow);
    }

    /**
     * Create and populate the sheet containing metadata about the variables.
     */
    private void createVariableMetadataSheet() {
        Map<String, Object> metadata = new LinkedHashMap<>();
        Sheet sheet = this.workBook.createSheet("Variable Metadata");
        int rowNumber = 1;
        int columnNumber = 0;
        Row row;
        Cell cell;

        metadata.put("Order", null);
        metadata.put("Variable", null);
        metadata.put("Label", null);
        metadata.put("Type", null);
        metadata.put("Length", null);
        metadata.put("Format", null);

        //Create first row
        row = sheet.createRow(0);

        //Get column headers
        for (String column : metadata.keySet()) {
            cell = row.createCell(columnNumber++);

            cell.setCellValue(column);
            cell.setCellStyle(styleHeader);
        }
        //Freeze the header row
        sheet.createFreezePane(0, 1, 0, 1);

        //Pupulate rows
        //Get the metadata from the map
        for (VariableMetadata variable : this.variableMetadata) {
            columnNumber = 0;

            metadata.put("Order", rowNumber);
            metadata.put("Variable", variable.name);
            metadata.put("Label", variable.label);
            metadata.put("Type", variable.type);
            metadata.put("Length", variable.length);
            metadata.put("Format", variable.format);

            //Move to next row
            row = sheet.createRow(rowNumber++);

            //Get values and write them
            for (Object value : metadata.values()) {
                cell = row.createCell(columnNumber++);
                cell.setCellValue(getSafely(value));
                cell.setCellStyle(rowNumber % 2 > 0 ? this.styleEvenRow : this.styleOddRow);
            }
        }
    }

    /**
     * Will not throw an exception if the object being retrieved is null.
     * @param object the object to check and get.
     * @return the String value of the object.
     */
    private static String getSafely(Object object) {
        return object == null ? null : object.toString();
    }

    /**
     * Writes the excel file to disc and closes the output stream.
     * Also deletes any temporary files used by POI.
     * @throws IOException
     */
    public void save() throws IOException {
        File excelFile = new File(outputPath, this.name + ".xlsx");
        FileOutputStream output = new FileOutputStream(excelFile);
        this.workBook.write(output);
        output.close();
        workBook.dispose();
    }

    /**
     * Instantiates the styles used in the sheets.
     */
    private void createStyle() {
        org.apache.poi.ss.usermodel.Font font = workBook.createFont();

        /*
         * POI is retarded so I'm making a note that this worked as of:
         * 10/24/2014 9:53 AM
         */
        XSSFColor headerBackroundColor = new XSSFColor(new Color(194, 214, 154));
        XSSFColor headerFontColor = new XSSFColor(new Color(79, 98, 40));
        XSSFColor oddRowColor = new XSSFColor(new Color(233, 240, 220));

        //Style Headers
        //Set Larger font
        font.setFontHeightInPoints((short) 11);
        font.setColor(headerFontColor.getIndexed());
        font.setFontName("Arial");
        font.setBold(true);
        styleHeader = (XSSFCellStyle) workBook.createCellStyle();
        styleHeader.setFont(font);
        styleHeader.setFillForegroundColor(headerBackroundColor);
        styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        //Set path font
        font = workBook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Arial");

        //Set up font for most cells
        font = workBook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Arial");

        //Style even rows
        styleEvenRow = (XSSFCellStyle) workBook.createCellStyle();
        styleEvenRow.setFont(font);

        //Style odd rows
        styleOddRow = (XSSFCellStyle) workBook.createCellStyle();
        styleOddRow.setFont(font);
        styleOddRow.setFillForegroundColor(oddRowColor);
        styleOddRow.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleOddRow.setBorderTop(BorderStyle.THIN);
        styleOddRow.setTopBorderColor(headerFontColor.getIndexed());
        styleOddRow.setBorderBottom(BorderStyle.THIN);
        styleOddRow.setBottomBorderColor(headerFontColor.getIndexed());
    }

    /**
     * Check if the file is truncated
     * @return truncated
     */
    public boolean isTruncated() {
        return this.truncated;
    }
}
