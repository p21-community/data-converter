/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter.destinations;

import org.apache.commons.lang.StringUtils;
import org.opencdisc.parsing.xml.XmlWriter;
import org.opencdisc.validator.data.DataRecord;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.transform.TransformerConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static org.apache.commons.lang3.time.DateFormatUtils.ISO_DATETIME_FORMAT;

public class XMLDestinationFormat extends AbstractDestinationFormat {

    private static final String ODM_NAMESPACE = "http://www.cdisc.org/ns/odm/v1.3";
    private static final String DATASET_XML_NAMESPACE = "http://www.cdisc.org/ns/Dataset-XML/v1.0";
    private static final String DATASET_XML_VERSION = "1.0";
    private static final String ODM_VERSION = "1.3.2";
    private static final String ORIGINATOR = "OpenCDISC Community Data Converter";
    /** The OID of the study */
    protected String studyOID;
    /** The MetaDataVersion of the study */
    protected String metaDataVersionOID;
    /** The define.xml used for lookups */
    protected Document defineXML;
    /** The output stream used in xml mapping */
    protected FileOutputStream outputStream;
    /** used for writing the xml file */
    protected XmlWriter writer;
    /** If the supplied define.xml document is version 2 or 1 */
    protected boolean isDefine2;
    /** The order of the ItemData tags in the document (starts at 1) */
    protected int sequenceNumber = 1;
    /** Map between the variable name and the OID for a particular ItemGroupDef from the define.xml */
    protected final HashMap<String, String> variableOIDMap;
    /** The OID for the File being written. This is the ItemGroupDef OID from the define.xml */
    protected final String itemGroupDefOid;

    /**
     * Set the output path for the converted file.
     *
     * @param outputPath the path to save the file.
     * @param document the define.xml document.
     * @param isDefine2 whether or not the define.xml is version 2.0 or not.
     * @param variableOIDs map between the variable name and the OID of that variable from the define.xml.
     *                     since <a href="https://pinnacle21.atlassian.net/browse/OPEN-186">OPEN-186</a>
     * @throws java.io.IOException
     */
    public XMLDestinationFormat(Document document, String outputPath, boolean isDefine2, String itemGroupDefOid, HashMap<String, String> variableOIDs) throws IOException {
        super(outputPath);
        this.defineXML = document;
        this.isDefine2 = isDefine2;
        this.variableOIDMap = variableOIDs;
        this.itemGroupDefOid = itemGroupDefOid;
    }

    @Override
    public boolean writeRecord(DataRecord record) throws IOException {

        if (this.writer == null) {
            try {

                this.outputStream = new FileOutputStream(new File(this.outputPath, this.name + ".xml"));
                this.writer = new XmlWriter(outputStream, ODM_NAMESPACE);

                //Check if the define.xml has the Study element otherwise make it the STUDYID from the xpt file.
                if (!(defineXML.getElementsByTagName("Study").getLength() == 0)) {
                    this.studyOID = ((Element) defineXML.getElementsByTagName("Study").item(0)).getAttributeNode("OID").getValue();
                } else {
                    this.studyOID = record.getValue("STUDYID").toString();
                }

                //Check if the define.xml has the MetaDataVersion element otherwise make it the study id from the sas files
                if (!(defineXML.getElementsByTagName("MetaDataVersion").getLength() == 0)) {
                    this.metaDataVersionOID = ((Element) defineXML.getElementsByTagName("MetaDataVersion").item(0)).getAttributeNode("OID").getValue();
                } else {
                    this.metaDataVersionOID = studyOID;
                }
                writeHeader();

            } catch (IOException | SAXException | TransformerConfigurationException e) {
                e.printStackTrace();
            }
        }

        try {

            XmlWriter.SimpleAttributes attributes;

            //Build the ItemGroupData element
            attributes = this.writer.newAttributes()
                    .addAttribute("ItemGroupOID", itemGroupDefOid)
                    .addAttribute("data:ItemGroupDataSeq", String.valueOf(sequenceNumber++));

            //Add to the file
            writer.startElement("ItemGroupData", attributes);

            //For each variable build the ItemData element
            XmlWriter.SimpleAttributes itemAttributes;

            for (VariableMetadata variable : this.variableMetadata) {
                if (StringUtils.isNotEmpty(record.getValue(variable.name).toString())) {

                    itemAttributes = this.writer.newAttributes()
                            .addAttribute("ItemOID", variableOIDMap.get(variable.name.toUpperCase()))
                            .addAttribute("Value", record.getValue(variable.name).toString());

                    //Add to the file
                    writer.simpleElement("ItemData", itemAttributes);

                }
            }

            //End the itemGroupData Element
            writer.endElement("ItemGroupData");

        } catch (SAXException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void close() throws Exception {
        writer.endElement("ClinicalData");
        writer.endElement("ODM");
        writer.close();
    }

    public void writeHeader() {
        XmlWriter.SimpleAttributes odmAttributes;
        XmlWriter.SimpleAttributes clinicalDataAttributes;

        //Attirbutes for the ODM element
        odmAttributes = this.writer.newAttributes()
                .addAttribute("xmlns:data", DATASET_XML_NAMESPACE)
                .addAttribute("FileType", "Snapshot")
                .addAttribute("ODMVersion", ODM_VERSION)
                .addAttribute("data:DatasetXMLVersion", DATASET_XML_VERSION)
                .addAttribute("FileOID",String.format(studyOID + "(%s)", itemGroupDefOid))
                .addAttribute("CreationDateTime",
                        ISO_DATETIME_FORMAT.format(System.currentTimeMillis()))
                .addAttribute("Originator", ORIGINATOR)
                .addAttribute("SourceSystem", "OpenCDISC Community")
                .addAttribute("SourceSystemVersion", "2.0");

        //Attributes for the Clinical Data element
        clinicalDataAttributes = this.writer.newAttributes()
                .addAttribute("StudyOID", studyOID)
                .addAttribute("MetaDataVersionOID", metaDataVersionOID);

        try {
            writer.startElement("ODM", odmAttributes);
            writer.startElement("ClinicalData", clinicalDataAttributes);
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get all of the variables for a file
     * @return this list of variables
     */
    public List<VariableMetadata> getVariables() {
        return  this.variableMetadata;
    }
}
