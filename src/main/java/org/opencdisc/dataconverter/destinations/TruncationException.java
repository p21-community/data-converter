package org.opencdisc.dataconverter.destinations;

public class TruncationException extends Exception {

    public String fileName;

    public TruncationException (String message, String fileName) {
        super((message));
        this.fileName = fileName;
    }

    public String getFileName() {return this.fileName;}
}
