/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter.destinations;

/**
 * Information about a variable in a SAS XPT file.
 * Contains basic information about the variable.
 *
 * @author Gerard
 */
public class VariableMetadata {

    public final String name;
    public final String label;
    public final String type;
    public final Double length;
    public final String format;

    /**
     * Construct a variable.
     * @param name the name
     * @param label the label
     * @param type the type
     * @param length the length
     * @param format the format
     */
    public VariableMetadata(String name, String label, String type, Double length, String format) {
        this.name = name;
        this.label = label;
        this.type = type;
        this.length = length;
        this.format = format;
    }
}
