/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter;

public interface ConverterListener {
    /**
     * Called by a <code>Converter</code> implementation when progress is made on a
     * process.
     *
     * @param event  a <code>ConverterEvent</code> with detailed information about the
     *     current progress
     */
    public void converterProgressUpdated(ConverterEvent event);

    /**
     * Called by a <code>Converter</code> implementation when a process starts.
     *
     * @param event a blank <code>ConverterEvent</code> object
     */
    public void converterStarted(ConverterEvent event);

    /**
     * Called by a <code>Converter</code> implementation when a process stops.
     *
     * @param event a blank <code>ConverterEvent</code> object
     */
    public void converterStopped(ConverterEvent event);
}
