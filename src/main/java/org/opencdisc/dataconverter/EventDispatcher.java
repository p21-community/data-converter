/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter;

import org.opencdisc.dataconverter.ConverterEvent;
import org.opencdisc.dataconverter.ConverterEvent.State;
import org.opencdisc.dataconverter.ConverterListener;

import java.util.LinkedHashSet;
import java.util.Set;

public class EventDispatcher {
    private final Set<ConverterListener> listeners =
        new LinkedHashSet<ConverterListener>();

    public synchronized void add(ConverterListener listener) {
        this.listeners.add(listener);
    }

    public synchronized void clear() {
        this.listeners.clear();
    }
//
//    public void fireParsing(String configurationPath) {
//        this.dispatchUpdated(new ConverterEvent(State.Parsing, configurationPath));
//    }

    public void fireTaskStart(String taskName, long currentTask, long totalTasks) {
        this.dispatchUpdated(new ConverterEvent(State.Processing, currentTask,
                totalTasks, taskName));
    }

    public void fireTaskIncrement(String taskName, long currentTask, long totalTasks) {
        ConverterEvent event = new ConverterEvent(State.Processing, currentTask,
                totalTasks, taskName);

//        event.setSubevent(new ConverterEvent(State.Processing, currentIncrement));

        this.dispatchUpdated(event);
    }

    public void fireTaskCompleted(String taskName, long currentTask, long totalTasks) {
        this.dispatchUpdated(new ConverterEvent(State.Completed, currentTask, totalTasks,
                taskName));
    }

//    public void fireSubtaskIncrement(String subtaskName, int currentIncrement) {
//        this.dispatchUpdated(new ConverterEvent(State.Subprocessing, currentIncrement,
//                subtaskName));
//    }
//
//    public void fireGenerating(String resultPath) {
//        this.dispatchUpdated(new ConverterEvent(State.Generating, resultPath));
//    }

    public void fireStarted() {
        this.dispatchStarted(new ConverterEvent());
    }

    public void fireStopped() {
        this.dispatchStopped(new ConverterEvent());
    }

    public synchronized boolean remove(ConverterListener listener) {
        return this.listeners.remove(listener);
    }

    private synchronized Set<ConverterListener> copyListeners() {
        return new LinkedHashSet<ConverterListener>(this.listeners);
    }

    private void dispatchUpdated(ConverterEvent event) {
        Set<ConverterListener> listeners = this.copyListeners();

        for (ConverterListener listener : listeners) {
            synchronized (listener) {
                listener.converterProgressUpdated(event);
            }
        }
    }

    private void dispatchStarted(ConverterEvent event) {
        Set<ConverterListener> listeners = this.copyListeners();

        for (ConverterListener listener : listeners) {
            synchronized (listener) {
                listener.converterStarted(event);
            }
        }
    }

    private void dispatchStopped(ConverterEvent event) {
        Set<ConverterListener> listeners = this.copyListeners();

        for (ConverterListener listener : listeners) {
            synchronized (listener) {
                listener.converterStopped(event);
            }
        }
    }
}
