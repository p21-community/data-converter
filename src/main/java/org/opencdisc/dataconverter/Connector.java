/*
 * Copyright © 2008-2014 Pinnacle 21 LLC
 *
 * This file is part of OpenCDISC Community.
 *
 * OpenCDISC Community is free software licensed under the OpenCDISC Open Source Software License
 * located at [http://www.opencdisc.org/license] (the "License").
 *
 * OpenCDISC Community is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY,
 * and is distributed "AS IS," "WITH ALL FAULTS," and without the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the License for more details.
 */

package org.opencdisc.dataconverter;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.opencdisc.dataconverter.destinations.*;
import org.opencdisc.ipc.Bridge;
import org.opencdisc.ipc.buffers.EventProtos.IpcControlEvent;
import org.opencdisc.ipc.buffers.EventProtos.IpcStateEvent;
import org.opencdisc.validator.SourceOptions;
import org.opencdisc.validator.data.DataSource;
import org.opencdisc.validator.data.EnhancedSasTransportDataSource;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import rx.Observer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Connector {
    private static final Logger logger = LogManager.getLogger(Connector.class);

    static void attach() {
        logger.info("Attaching to IPC bridge...");
        Bridge.start().subscribe(new Observer<IpcControlEvent>() {
            public void onCompleted() {
                logger.info("IPC event stream terminated");

                this.stop();
            }

            public void onError(Throwable ex) {
                logger.error("Unexpected exception in IPC event emitter", ex);

                this.stop();
            }

            public void onNext(IpcControlEvent event) {
                IpcControlEvent.Control code = event.getControl();

                logger.info("Received new control event of type {" + code.name() + "}");

                try {
                    if (code == IpcControlEvent.Control.START) {
                        this.start(event);
                    } else if (code == IpcControlEvent.Control.ABORT) {
                        logger.debug("Attempting to abort validation");

                        this.stop();
                    }
                } catch (Exception ex) {
                    logger.error("Unexpected exception acting on control event", ex);

                    Bridge.onException(ex);
                }
            }

            private void start(IpcControlEvent event) throws Exception {
                JsonObject parameters = JsonObject.readFrom(event.getJson());
                JsonObject sourceParameters = parameters.get("sources").asObject();
                String destination = asAbsolutePath(parameters.get("target"));

                //Validate the output path (so it will create the path in case they type it in)
                ArgumentParser.validateOutputPath(new File(destination));

                String type = parameters.get("type").asString();
                IpcStateEvent message;

                int index = 1;
                JsonArray paths = sourceParameters.get("paths").asArray();

                SourceOptions options;

                //If the output type is Dataset-XML we need to ensure that the domains and variables are located in the define.
                //TODO figure out how to not run through the file twice...
                //The define.xml file that was passed in
                File define;
                //The define.xml after being loaded into DOM
                Document defineXML = null;
                //NodeList of itemGroupDefs from the define.xml
                NodeList itemGroupDefList = null;
                //Map between the filename and the OID from the define.xml for that filename
                HashMap<String, String> itemGroupDefMap = null;
                //Complete Map between each ItemGroupDef's name then a map between each variable name and the OID from the ItemGroupDef
                HashMap<String, HashMap<String, String>> itemDefMap = null;
                //If the define.xml is version 1 or 2
                boolean isDefine2 = false;
                //If the validation of datasetXML fails this will be true
                //FIXME figure out how to not use this;
                //FIXME because if I don't do this, it keeps running through the rest of the code
                boolean aborted = false;

                //Dataset-XML specific validation
                if (type.equals("Dataset XML")) {
                    define = asFile(parameters.get("define"));
                    defineXML = ArgumentParser.parseFile(define);
                    itemGroupDefList = ArgumentParser.getItemGroupDefs(defineXML);
                    itemGroupDefMap = ArgumentParser.getItemGroupDefMap(itemGroupDefList);
                    itemDefMap =  ArgumentParser.getItemDefMap(defineXML, itemGroupDefList);
                    isDefine2 = ArgumentParser.getAttributes(defineXML).get("Define Version").equals("2");

                    for (JsonValue value : paths) {
                        options = SourceOptions.fromFile(asFile(value))
                                .setType(SourceOptions.Type.SasTransport);

                        //Construct the "Validating" Message
                        //TODO is emitting TASK_SUBPROCESSING alright?
                        message = IpcStateEvent.newBuilder()
                                .setState(IpcStateEvent.State.TASK_SUBPROCESSING)
                                .setLabel(options.getSubname())
                                .setCurrent(index)
                                .setTotal(paths.values().size())
                                .build();

                        Bridge.emit(message);

                        try (DataSource source = new EnhancedSasTransportDataSource(options)) {
                            ArgumentParser.validateXPTFile(asFile(value), source, itemGroupDefMap.keySet(),
                                    itemDefMap);

                            index++;
                        } catch (IOException ex) {
                            logger.error("Dataset validation exception", ex);
                            Bridge.onComplete(new JsonObject().add("Information", ex.getMessage()));
                            aborted = true;
                            break;
                        } catch (Exception ex) {
                            logger.error("Unexpected exception", ex);
                        }
                    }
                    index = 1;
                }

                //Begin usual logic
                //TODO figure out how to avoid this logic if the validation fails WITHOUT a boolean.
                if (!aborted) {
                    List<String> truncatedFiles = new ArrayList<>();

                    for (JsonValue value : paths) {
                        options = SourceOptions.fromFile(asFile(value))
                                .setType(SourceOptions.Type.SasTransport);

                        message = IpcStateEvent.newBuilder()
                                .setState(IpcStateEvent.State.TASK_PROCESSING)
                                .setLabel(options.getSubname())
                                .setCurrent(index)
                                .setTotal(paths.values().size())
                                .build();

                        Bridge.emit(message);

                        try (DataSource source = new EnhancedSasTransportDataSource(options)) {
                            DestinationFormat target = null;

                            switch (type) {
                                case "Excel":
                                    target = new ExcelDestinationFormat(destination);
                                    break;
                                case "CSV":
                                    target = new CSVDestinationFormat(destination);
                                    break;
                                case "Dataset XML":
                                    String fileName = FilenameUtils.getBaseName(asFile(value).getName()).toUpperCase();
                                    String itemGroupDefOID = ArgumentParser.getItemGroupDefMap(itemGroupDefList).get(fileName);
                                    target = new XMLDestinationFormat(defineXML, destination, isDefine2, itemGroupDefOID, itemDefMap.get(fileName));
                                    break;
                            }

                            Converter.convert(source, target);
                        } catch (TruncationException ex) {
                            truncatedFiles.add(ex.getFileName());
                        } catch (Exception ex) {
                            logger.error("Unexpected exception", ex);
                        }

                        message = IpcStateEvent.newBuilder()
                                .setState(IpcStateEvent.State.TASK_COMPLETED)
                                .setLabel(options.getSubname())
                                .setCurrent(index)
                                .setTotal(paths.values().size())
                                .build();

                        Bridge.emit(message);

                        ++index;
                    }

                    if (truncatedFiles.size() != 0) {
                        String truncationMessage = truncatedFiles.size() > 1 ?
                                "Some files were truncated to 1,048,575 records because the XPT files were too large." :
                                (truncatedFiles.get(0) + " was truncated to 1,048,575 records because the XPT file was too large.");
                        Bridge.onComplete(new JsonObject().add("Truncation", truncationMessage));
                    } else {
                        Bridge.onComplete();
                    }
                }
            }

            private void stop() {
                /*this.converter.abort();*/
            }

        });

        logger.info("Subscription ended, exiting program");
    }

    private static String asAbsolutePath(JsonValue value) {
        if (value == null || value.isNull()) {
            return null;
        }

        return asFile(value).getAbsolutePath();
    }

    private static File asFile(JsonValue value) {
        if (value == null || value.isNull()) {
            return null;
        }

        return new File(value.asString());
    }
}
